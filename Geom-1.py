import math
#  File: Geom.py

#  Description: Performs simple calculations and analysis of points and lines

#  Student Name: Christopher Calizzi

#  Student UT EID:csc3322

#  Course Name: CS 313E

#  Unique Number: 50295

#  Date Created: 2-10-20

#  Date Last Modified: 2-14-20

class Point(object):
    # constructor with default values
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    # get distance to other which is another Point object
    def dist(self, other):
        return math.sqrt((self.x-other.x)**2 + (self.y-other.y)**2)

    # create a string representation of a Point (x, y)
    def __str__(self):
        return "(" + "{:.2f}".format(self.x)+"," + "{:.2f}".format(self.y) +")"

    # test for equality between two points
    def __eq__(self, other):
        return abs(self.x - other.x) < 1.0e-6 and abs(self.y - other.y)<1.0e-6


class Line(object):
    # line is defined by two Point objects p1 and p2
    # constructor assign default values if user does not define
    # the coordinates of p1 and p2 or the two points are the same
    def __init__(self, p1_x=0, p1_y=0, p2_x=1, p2_y=1):
        self.p1 = Point(p1_x,p1_y)
        self.p2 = Point(p2_x,p2_y)
        if self.p1 == self.p2:
            if not is_equal(self.p1.x,0):
                self.p1 = Point(0,0)
            else:
                self.p1 = Point(1,1)

    # returns True if the line is parallel to the x axis
    # and False otherwise
    def is_parallel_x(self):
        return is_equal(self.p1.y,self.p2.y)
    # returns True if the line is parallel to the y axis
    # and False otherwise
    def is_parallel_y(self):
        return is_equal(self.p1.x,self.p2.x)
    # determine slope for the line
    # return float ('inf') if line is parallel to the y-axis
    def slope(self):
        if (is_equal(self.p1.x,self.p2.x)):
            return float('inf')
        else:
            return ((self.p2.y-self.p1.y)/(self.p2.x-self.p1.x))

    # determine the y-intercept of the line
    # return None if line is parallel to the y axis
    def y_intercept(self):
        if self.is_parallel_y():
            return None
        else:
            return self.p2.y-self.p2.x*self.slope()

    # determine the x-intercept of the line
    # return None if line is parallel to the x axis
    def x_intercept(self):
        if self.is_parallel_x():
            return None
        else:
            return self.p2.x - self.p2.y / self.slope()
    # returns True if line is parallel to other and False otherwise
    def is_parallel(self, other):
        if self.is_parallel_y() and other.is_parallel_y():
            return True
        else:
            return is_equal(self.slope(),other.slope())
    # returns True if line is perpendicular to other and False otherwise
    def is_perpendicular(self, other):
        if other.is_parallel_x():
            return self.is_parallel_y()
        else:
            return is_equal(self.slope(),-1/other.slope())
    # returns True if Point p is on the line or an extension of it
    # and False otherwise
    def is_on_line(self, p):
        return is_equal(p.y,p.x*self.slope()+self.y_intercept())
    # determine the perpendicular distance of Point p to the line
    # return 0 if p is on the line
    def perp_dist(self, p):
        if self.is_parallel_x():
            return abs(p.y - self.p1.y)
        elif self.is_parallel_y():
            return abs(p.x-self.p1.x)
        else:
            px = (p.x + self.get_x(p.y)) / 2
            py = (p.y + self.get_y(p.x)) / 2
            return p.dist(Point(px, py))
    # returns a Point object which is the intersection point of line
    # and other or None if they are parallel
    def intersection_point(self, other):
        if self.is_parallel(other):
            return None
        elif is_equal(self.p1.x,self.p2.x):
            return Point(self.p1.x,other.get_y(self.p1.x))
        elif is_equal(other.p1.x,other.p2.x):
            return Point(other.p1.x, self.get_y(other.p1.x))
        else:
            x_intersect = (self.y_intercept()-other.y_intercept())/(other.slope()-self.slope())
            y_intersect = self.get_y(x_intersect)
            return Point(x_intersect,y_intersect)
    # return True if two points are on the same side of the line
    # and neither points are on the line
    # return False if one or both points are on the line or both
    # are on the same side of the line
    def on_same_side(self, p1, p2):
        if is_equal(self.p1.x,self.p2.x):
            return (p1.x<self.p1.x and p2.x<self.p1.x) or (p1.x>self.p1.x and p2.x>self.p1.x)
        else:
            above1 = self.get_y(p1.x)<p1.y
            above2 = self.get_y(p2.x)<p2.y
            equal1 = is_equal(self.get_y(p1.x),p1.y)
            equal2 = is_equal(self.get_y(p2.x),p2.y)
            return (not (above1^above2)) and (not (equal1 or equal2))
    # string representation of the line - one of three cases
    # y = c if parallel to the x axis
    # x = c if parallel to the y axis
    # y = m * x + b
    def __str__(self):
        if self.is_parallel_x():
            return "y = " + "{:.2f}".format(self.p1.y)
        elif self.is_parallel_y():
            return "x = " + "{:.2f}".format(self.p1.x)
        else:
            return "y = " + "{:.2f}".format(self.slope()) + " * x + " + "{:.2f}".format(self.y_intercept())
    # calculates y values based on slope and intercept of line
    def get_y(self,x):
        if not (self.is_parallel_y()):
            return x*self.slope() + self.y_intercept()
        else:
            return None

    # calculates x values based on slope and intercept of line
    def get_x(self,y):
        if not (self.is_parallel_x()):
            return (y-self.y_intercept())/self.slope()
        else:
            return None

#compares 2 floats and determines whether they are equal
def is_equal(x,y):
    return (abs(x-y)<1.0e-6)

def main():
    # open file "geom.txt" for reading
    in_file = open("geom.txt","r")
    # read the coordinates of the first Point P
    line = in_file.readline()
    line = line.strip()
    splits = line.split()
    P = Point(float(splits[0]),float(splits[1]))
    # read the coordinates of the second Point Q
    line = in_file.readline()
    line = line.strip()
    splits = line.split()
    Q = Point(float(splits[0]), float(splits[1]))
    # print the coordinates of points P and Q
    print("Coordinates of P:",P)
    print("Coordinates of Q:",Q)
    # print distance between P and Q
    print("Distance between P and Q:","{:.2f}".format(P.dist(Q)))
    # print the slope of the line PQ
    PQ = Line(P.x,P.y,Q.x,Q.y)
    print("Slope of PQ:","{:.2f}".format(PQ.slope()))
    # print the y-intercept of the line PQ
    if not PQ.is_parallel_y():
        print("Y-Intercept of PQ:","{:.2f}".format(PQ.y_intercept()))
    elif is_equal(PQ.p1.x,0.0):
        print("PQ is the Y-Axis")
    else:
        print("PQ has no Y intercept")
    # print the x-intercept of the line PQ
    if not PQ.is_parallel_x():
        print("X-Intercept of PQ:","{:.2f}".format(PQ.x_intercept()))
    elif is_equal(PQ.p1.y,0.0):
        print("PQ is the X-Axis")
    else:
        print("PQ has no x intercept")
    # read the coordinates of the third Point A
    line = in_file.readline()
    line = line.strip()
    splits = line.split()
    A = Point(float(splits[0]), float(splits[1]))
    # read the coordinates of the fourth Point B
    line = in_file.readline()
    line = line.strip()
    splits = line.split()
    B = Point(float(splits[0]), float(splits[1]))
    # print the string representation of the line AB
    AB = Line(A.x,A.y,B.x,B.y)
    print("Line AB:",AB)
    # print if the lines PQ and AB are parallel or not
    if PQ.is_parallel(AB):
        print("PQ is parallel to AB")
    else:
        print("PQ is not parallel to AB")
    # print if the lines PQ and AB (or extensions) are perpendicular or not
    if PQ.is_perpendicular(AB):
        print("PQ is perpendicular to AB")
    else:
        print("PQ is not perpendicular to AB")
    # print coordinates of the intersection point of PQ and AB if not parallel
    if not PQ.is_parallel(AB):
        print("Intersection point of PQ and AB:",PQ.intersection_point(AB))
    else:
        print("PQ and AB are parallel and do not intersect")
    # read the coordinates of the fifth Point G
    line = in_file.readline()
    line = line.strip()
    splits = line.split()
    G = Point(float(splits[0]), float(splits[1]))
    # read the coordinates of the sixth Point H
    line = in_file.readline()
    line = line.strip()
    splits = line.split()
    H = Point(float(splits[0]), float(splits[1]))
    # print if the the points G and H are on the same side of PQ
    if PQ.on_same_side(G,H):
        print("G and H are on the same side of PQ")
    else:
        print("G and H are not on the same side of PQ")
    # print if the the points G and H are on the same side of AB
    if AB.on_same_side(G,H):
        print("G and H are on the same side of AB")
    else:
        print("G and H are not on the same side of AB")
    # close file "geom.txt"
    in_file.close()
if __name__ == "__main__":
    main()